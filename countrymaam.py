# -*- coding: utf-8 -*-
"""
Created on Wed Feb 10 23:36:12 2021

@author: mshim
"""
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np

#　原料費の読み込み
df=pd.read_csv("CMOHistoricalDataAnnual.csv")
#　カントリーマアムのデータ
df_countrymaam=pd.read_csv("countrymaam.csv",encoding = "shift-jis")
df=df.iloc[45:57]
#print(df_countrymaam)

df=pd.merge(df,df_countrymaam,on="Year",how="left")
#print(df.info())

#2005年で規格化
Columnlist=df.columns.tolist()
Columnlist.remove("Year")
for i in Columnlist:
    #print(df[i][0])
    #..を0に変換
    df[i].replace("..",0,inplace=True)
    # 数字だけどstringがあるのでfloatに変換
    df[i]=df[i].astype('float64')
    df[i]=df[i]/df[i][0]

print(df["COCOA"])

sns.lineplot(data=df, x='Year', y='SUGAR_US', ci='sd')
sns.lineplot(data=df, x='Year', y='PALM_OIL', ci='sd')
sns.lineplot(data=df, x='Year', y='total weight', ci='sd')
#sns.pairplot(df, hue="species", size=2.5)
# plt.plot(df["Year"],df["SUGAR_US"])
# plt.plot(df["Year"],df["PALM_OIL"])
# plt.plot(df["Year"],df["total weight"])
plt.legend()
plt.show()
# #df.to_csv("result.csv")


